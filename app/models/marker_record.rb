class MarkerRecord < ActiveRecord::Base
  belongs_to :user
  belongs_to :external_device

  validate :check_exists_for_device_and_eid, on: :create

  #before_save :convert_timestamp_to_datetime_string

  def to_hash
    {

    }
  end

  def check_exists_for_device_and_eid
    marker_count = MarkerRecord.where(
        eid: eid,
        external_device_id: external_device.id
    ).count

    if marker_count != 0
      errors.add(:eid, "already exists for this device")
    end
  end

  def convert_timestamp_to_datetime_string
    self.date_correlated = DateTime.strptime(self.date_correlated.to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
    self.external_date_created = DateTime.strptime(self.external_date_created.to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
  end
end
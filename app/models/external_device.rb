class ExternalDevice < ActiveRecord::Base
  has_many :marker_records

  def last_eid
    last_marker = marker_records.order(eid: :asc).last
    {
        eid: last_marker.nil? ? -1 : last_marker.eid
    }
  end

  def to_hash
    {
        id: self.id,
        uuid: self.uuid,
        os: self.os,
        os_version: self.os_version,
        device_make: self.device_make,
        device_model: self.device_model
    }
  end
end
class ExternalDevicesController < ApplicationController
  def new
    new_uuid = {
        responseId: 1,
        uuid: SecureRandom.uuid
    }

    response_json_200(new_uuid)
  end

  def create
    external_device = ExternalDevice.new params[:external_device].permit(:uuid, :os, :os_version, :device_make, :device_model)

    if external_device.save
      response_merge_json_200(external_device, { responseId: 2 })
    else
      response_error_json_200({
          error: 'Server error'
                              })
    end

  end

  def show
    external_device = ExternalDevice.find_by_uuid(params[:id])
    if external_device.nil?
      response_error_json_200({
          error: 'No external device found'
                              })
    else
      response_merge_json_200(external_device, { responseId: 3 })
    end

  end
end

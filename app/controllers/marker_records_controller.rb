class MarkerRecordsController < ApplicationController
  before_filter :get_external_device

  def show
  end

  def create
    marker_record = MarkerRecord.new params[:marker_record].permit(:latitude, :longitude, :date_correlated, :external_date_created, :title, :accuracy, :altitude, :bearing, :speed, :eid, :extra_id)
    marker_record.external_device = @external_device
    marker_record.date_correlated = DateTime.strptime(params[:date_correlated].to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
    marker_record.external_date_created = DateTime.strptime(params[:external_date_created].to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')

    if marker_record.save
      response_merge_json_200(marker_record, {responseId: 4})
    else
      response_error_json_200({
                                  error: 'Did not save marker record.'
                              })
    end
  end

  def new
    #response_json_200(@external_device.marker_records.last)
    response_merge_json_200(@external_device.last_eid, {responseId: 5})
  end

  def batch
    marker_batch_params = params.permit(:marker_batch => [:latitude, :longitude, :date_correlated, :external_date_created, :title, :accuracy, :altitude, :bearing, :speed, :eid, :extra_id])
    puts "MARKER BATCH PARAMS"
    puts marker_batch_params.inspect
    marker_hash_array = marker_batch_params[:marker_batch].map {|m|
      m[:external_device_id] = @external_device.id
      m[:date_correlated] = DateTime.strptime(m[:date_correlated].to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
      m[:external_date_created] = DateTime.strptime(m[:external_date_created].to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
      m
    }

    puts "MARKER HASH"
    puts marker_hash_array.inspect
    MarkerRecord.create marker_hash_array
    #MarkerRecord.create(params[:marker_batch]) do |m|
    #  m.external_device_id = @external_device.id
    #  m.date_correlated = DateTime.strptime(m[:date_correlated].to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
    #  m.external_date_created = DateTime.strptime(m[:external_date_created].to_s[0..-4],'%s').strftime('%Y-%m-%d %H:%M:%S')
    #end

    response_merge_json_200(@external_device, {responseId: 6})
  end

  private
  def get_external_device
    if params[:external_device_id]
      @external_device = ExternalDevice.find_by_uuid(params[:external_device_id])
      if @external_device.nil?
        response_error_json_200({
                                    error: 'Did not find external device.'
                                })
      end
    else
      response_error_json_200({
                                  error: 'Invalid request.'
                              })
    end
  end
end

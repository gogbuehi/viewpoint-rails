class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception

  protected
  def response_with_200(message,data={},status="ok")
    respond_to do |format|
      msg = { status: status, message: message, data: data }
      format.json { render json: msg }
    end
  end

  def response_json_200(data={})
    respond_to do |format|
      format.json { render json: data }
    end
  end

  def response_with_error(message,status="Error")
    respond_to do |format|
      msg = { status: status, message: message }
      format.json  { render json: msg, status: 403 } # don't do msg.to_json
    end
  end

  def response_merge_json_200(data_object, merge_data={})
    response_json_200(data_object.to_hash.merge(merge_data))
  end

  def response_error_json_200(merge_data={})
    response_json_200(merge_data.merge({responseId: -1}))
  end
end

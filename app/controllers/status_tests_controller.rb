class StatusTestsController < ApplicationController
  def index
    @status_tests = StatusTest.first
    response_json_200(@status_tests)
  end

  def create
    data = params[:data]
    status_test = StatusTest.new
    status_test.data = data
    if status_test.save!
      response_json_200(status_test)
    else
      response_with_error("Bad request")
    end
  end

  def show
    status_test = StatusTest.find_by_id(params[:id])
    if status_test
      response_json_200(status_test)
    else
      response_with_error("Bad request")
    end
  end

  def delete
  end

  def update
  end


end

class CreateExternalDevices < ActiveRecord::Migration
  def change
    create_table :external_devices do |t|
      t.string :uuid
      t.string :os
      t.string :os_version
      t.string :device_make
      t.string :device_model

      t.timestamps
    end
    add_index :external_devices, :uuid,                unique: true
  end
end

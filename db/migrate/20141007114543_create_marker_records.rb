class CreateMarkerRecords < ActiveRecord::Migration
  def change
    create_table :marker_records do |t|
      t.float :latitude
      t.float :longitude
      t.datetime :date_correlated
      t.datetime :external_date_created
      t.string :title
      t.float :accuracy
      t.float :altitude
      t.float :bearing
      t.float :speed
      t.integer :eid
      t.integer :external_device_id
      t.integer :user_id

      t.timestamps
    end
  end
end

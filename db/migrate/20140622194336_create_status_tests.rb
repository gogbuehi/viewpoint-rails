class CreateStatusTests < ActiveRecord::Migration
  def change
    create_table :status_tests do |t|
      t.string :data

      t.timestamps
    end
  end
end

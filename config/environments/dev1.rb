Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this).
  config.serve_static_assets = true

  # Compress JavaScripts and CSS.
  config.assets.css_compressor = :scss

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # Generate digests for assets URLs.
  config.assets.digest = true

  # Version of your assets, change this if you want to expire all your assets.
  config.assets.version = '1.0'

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Set to :debug to see everything in the log.
  config.log_level = :debug

  # Use a different logger for distributed setups.
  config.logger = Logger.new(STDOUT)

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  config.time_zone = 'America/Los_Angeles'

  config.action_mailer.default_url_options = { :host => 'gogbuehi.com' }

  #config.action_mailer.delivery_method = :smtp
  #
  #ActionMailer::Base.smtp_settings = {
  #    :user_name => ENV['MAIL_USER'],
  #    :password => ENV['MAIL_PASS'],
  #    :domain => 'dev1.slingshotcareers.com',
  #    :address => 'smtp.sendgrid.net',
  #    :port => 587,
  #    :authentication => :plain,
  #    :enable_starttls_auto => true
  #}
  #PUBLIC_URL = 'dev.slingshotcareers.com'
end